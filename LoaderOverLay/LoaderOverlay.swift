//
//  LoaderOverlay.swift
//  Tawouk
//
//  Created by Ismael AlShabrawy on 11/23/17.
//  Copyright © 2017 Baianat. All rights reserved.
//

import Foundation
import Lottie

public class LoaderOverlay{
    
    var LoaderoverlayView = UIView()

    let animationView = LOTAnimationView(name: "Loader")
    
    
    class var shared: LoaderOverlay {
        struct Static {
            static let instance: LoaderOverlay = LoaderOverlay()
        }
        return Static.instance
    }
    
    
    public func showOverlay(view: UIView!) {
        LoaderoverlayView = UIView(frame: UIScreen.main.bounds)
        LoaderoverlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        animationView.center = LoaderoverlayView.center
        LoaderoverlayView.addSubview(animationView)
        animationView.play()
        view.addSubview(LoaderoverlayView)
    }
    

    public func hideOverlayView() {
        animationView.stop()
        LoaderoverlayView.removeFromSuperview()
    }
    
}
